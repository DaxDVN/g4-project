﻿using System;
using System.Collections.Generic;

namespace G4.Models
{
    public partial class Booking
    {
        public int BookId { get; set; }
        public int? TripId { get; set; }
        public string? Name { get; set; }
        public string? SeatStatus { get; set; }
        public double? Amount { get; set; }

        public virtual Trip? Trip { get; set; }
    }
}
