﻿using System;
using System.Collections.Generic;

namespace G4.Models
{
    public partial class Train
    {
        public Train()
        {
            Wagons = new HashSet<Wagon>();
        }

        public int TrainId { get; set; }
        public string? Name { get; set; }
        public int? TypeId { get; set; }

        public virtual Type? Type { get; set; }
        public virtual ICollection<Wagon> Wagons { get; set; }
    }
}
