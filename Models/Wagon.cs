﻿using System;
using System.Collections.Generic;

namespace G4.Models
{
    public partial class Wagon
    {
        public Wagon()
        {
            Trips = new HashSet<Trip>();
        }

        public int WagonId { get; set; }
        public string? Name { get; set; }
        public int? Row { get; set; }
        public int? Col { get; set; }
        public int? TrainId { get; set; }
        public int? ClassId { get; set; }

        public virtual TicketClass? Class { get; set; }
        public virtual Train? Train { get; set; }
        public virtual ICollection<Trip> Trips { get; set; }
    }
}
