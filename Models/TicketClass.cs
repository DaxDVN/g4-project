﻿using System;
using System.Collections.Generic;

namespace G4.Models
{
    public partial class TicketClass
    {
        public TicketClass()
        {
            Wagons = new HashSet<Wagon>();
        }

        public int ClassId { get; set; }
        public string? Class { get; set; }

        public virtual ICollection<Wagon> Wagons { get; set; }
    }
}
