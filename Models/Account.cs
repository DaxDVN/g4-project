﻿using System;
using System.Collections.Generic;

namespace G4.Models
{
    public partial class Account
    {
        private static bool isLoggedIn { get; set; } = false;
        public string Username { get; set; } = null!;
        public string? Password { get; set; }
        public bool? Role { get; set; }

        public bool GetAccountStatus => isLoggedIn;

        public void Login ()
        {
            isLoggedIn = true;
        }
        public void Logout ()
        {
            isLoggedIn = false;
        }
    }
}
