﻿using G4.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace G4.Controllers
{
    public class WagonsController : Controller
    {
        private readonly PRN211Context _context;

        public WagonsController(PRN211Context context)
        {
            _context = context;
        }

        // GET: Wagons
        public async Task<IActionResult> Index()
        {
            var pRN211Context = _context.Wagons.Include(w => w.Class).Include(w => w.Train);
            return View(await pRN211Context.ToListAsync());
        }

        public async Task<IActionResult> BookSeat()
        {
            return View();
        }

        // GET: Wagons/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Wagons == null)
            {
                return NotFound();
            }

            var wagon = await _context.Wagons
                .Include(w => w.Class)
                .Include(w => w.Train)
                .FirstOrDefaultAsync(m => m.WagonId == id);
            if (wagon == null)
            {
                return NotFound();
            }

            return View(wagon);
        }

        // GET: Wagons/Create
        public IActionResult Create()
        {
            ViewData["ClassId"] = new SelectList(_context.TicketClasses, "ClassId", "ClassId");
            ViewData["TrainId"] = new SelectList(_context.Trains, "TrainId", "TrainId");
            return View();
        }

        // POST: Wagons/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("WagonId,Name,Row,Col,TrainId,ClassId")] Wagon wagon)
        {
            if (ModelState.IsValid)
            {
                _context.Add(wagon);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClassId"] = new SelectList(_context.TicketClasses, "ClassId", "ClassId", wagon.ClassId);
            ViewData["TrainId"] = new SelectList(_context.Trains, "TrainId", "TrainId", wagon.TrainId);
            return View(wagon);
        }

        // GET: Wagons/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Wagons == null)
            {
                return NotFound();
            }

            var wagon = await _context.Wagons.FindAsync(id);
            if (wagon == null)
            {
                return NotFound();
            }
            ViewData["ClassId"] = new SelectList(_context.TicketClasses, "ClassId", "ClassId", wagon.ClassId);
            ViewData["TrainId"] = new SelectList(_context.Trains, "TrainId", "TrainId", wagon.TrainId);
            return View(wagon);
        }

        // POST: Wagons/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("WagonId,Name,Row,Col,TrainId,ClassId")] Wagon wagon)
        {
            if (id != wagon.WagonId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(wagon);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!WagonExists(wagon.WagonId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["ClassId"] = new SelectList(_context.TicketClasses, "ClassId", "ClassId", wagon.ClassId);
            ViewData["TrainId"] = new SelectList(_context.Trains, "TrainId", "TrainId", wagon.TrainId);
            return View(wagon);
        }

        // GET: Wagons/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Wagons == null)
            {
                return NotFound();
            }

            var wagon = await _context.Wagons
                .Include(w => w.Class)
                .Include(w => w.Train)
                .FirstOrDefaultAsync(m => m.WagonId == id);
            if (wagon == null)
            {
                return NotFound();
            }

            return View(wagon);
        }

        // POST: Wagons/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Wagons == null)
            {
                return Problem("Entity set 'PRN211Context.Wagons'  is null.");
            }
            var wagon = await _context.Wagons.FindAsync(id);
            if (wagon != null)
            {
                _context.Wagons.Remove(wagon);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool WagonExists(int id)
        {
            return (_context.Wagons?.Any(e => e.WagonId == id)).GetValueOrDefault();
        }
    }
}
