﻿using G4.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace G4.Controllers
{
    public class TicketClassesController : Controller
    {
        private readonly PRN211Context _context;

        public TicketClassesController(PRN211Context context)
        {
            _context = context;
        }

        // GET: TicketClasses
        public async Task<IActionResult> Index()
        {
            return _context.TicketClasses != null ?
                        View(await _context.TicketClasses.ToListAsync()) :
                        Problem("Entity set 'PRN211Context.TicketClasses'  is null.");
        }

        // GET: TicketClasses/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.TicketClasses == null)
            {
                return NotFound();
            }

            var ticketClass = await _context.TicketClasses
                .FirstOrDefaultAsync(m => m.ClassId == id);
            if (ticketClass == null)
            {
                return NotFound();
            }

            return View(ticketClass);
        }

        // GET: TicketClasses/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: TicketClasses/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ClassId,Class")] TicketClass ticketClass)
        {
            if (ModelState.IsValid)
            {
                _context.Add(ticketClass);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(ticketClass);
        }

        // GET: TicketClasses/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.TicketClasses == null)
            {
                return NotFound();
            }

            var ticketClass = await _context.TicketClasses.FindAsync(id);
            if (ticketClass == null)
            {
                return NotFound();
            }
            return View(ticketClass);
        }

        // POST: TicketClasses/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ClassId,Class")] TicketClass ticketClass)
        {
            if (id != ticketClass.ClassId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(ticketClass);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TicketClassExists(ticketClass.ClassId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(ticketClass);
        }

        // GET: TicketClasses/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.TicketClasses == null)
            {
                return NotFound();
            }

            var ticketClass = await _context.TicketClasses
                .FirstOrDefaultAsync(m => m.ClassId == id);
            if (ticketClass == null)
            {
                return NotFound();
            }

            return View(ticketClass);
        }

        // POST: TicketClasses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.TicketClasses == null)
            {
                return Problem("Entity set 'PRN211Context.TicketClasses'  is null.");
            }
            var ticketClass = await _context.TicketClasses.FindAsync(id);
            if (ticketClass != null)
            {
                _context.TicketClasses.Remove(ticketClass);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TicketClassExists(int id)
        {
            return (_context.TicketClasses?.Any(e => e.ClassId == id)).GetValueOrDefault();
        }
    }
}
