﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using G4.Models;

namespace G4.Controllers
{
    public class RouteTrainsController : Controller
    {
        private readonly PRN211Context _context;

        public RouteTrainsController(PRN211Context context)
        {
            _context = context;
        }

        // GET: RouteTrains
        public async Task<IActionResult> Index()
        {
            var pRN211Context = _context.RouteTrains.Include(r => r.End).Include(r => r.Start);
            return View(await pRN211Context.ToListAsync());
        }

        // GET: RouteTrains/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.RouteTrains == null)
            {
                return NotFound();
            }

            var routeTrain = await _context.RouteTrains
                .Include(r => r.End)
                .Include(r => r.Start)
                .FirstOrDefaultAsync(m => m.RouteId == id);
            if (routeTrain == null)
            {
                return NotFound();
            }

            return View(routeTrain);
        }

        // GET: RouteTrains/Create
        public IActionResult Create()
        {
            ViewData["EndId"] = new SelectList(_context.Locations, "LocationId", "LocationId");
            ViewData["StartId"] = new SelectList(_context.Locations, "LocationId", "LocationId");
            return View();
        }

        // POST: RouteTrains/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("RouteId,StartId,EndId")] RouteTrain routeTrain)
        {
            if (ModelState.IsValid)
            {
                _context.Add(routeTrain);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["EndId"] = new SelectList(_context.Locations, "LocationId", "LocationId", routeTrain.EndId);
            ViewData["StartId"] = new SelectList(_context.Locations, "LocationId", "LocationId", routeTrain.StartId);
            return View(routeTrain);
        }

        // GET: RouteTrains/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.RouteTrains == null)
            {
                return NotFound();
            }

            var routeTrain = await _context.RouteTrains.FindAsync(id);
            if (routeTrain == null)
            {
                return NotFound();
            }
            ViewData["EndId"] = new SelectList(_context.Locations, "LocationId", "LocationId", routeTrain.EndId);
            ViewData["StartId"] = new SelectList(_context.Locations, "LocationId", "LocationId", routeTrain.StartId);
            return View(routeTrain);
        }

        // POST: RouteTrains/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("RouteId,StartId,EndId")] RouteTrain routeTrain)
        {
            if (id != routeTrain.RouteId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(routeTrain);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!RouteTrainExists(routeTrain.RouteId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["EndId"] = new SelectList(_context.Locations, "LocationId", "LocationId", routeTrain.EndId);
            ViewData["StartId"] = new SelectList(_context.Locations, "LocationId", "LocationId", routeTrain.StartId);
            return View(routeTrain);
        }

        // GET: RouteTrains/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.RouteTrains == null)
            {
                return NotFound();
            }

            var routeTrain = await _context.RouteTrains
                .Include(r => r.End)
                .Include(r => r.Start)
                .FirstOrDefaultAsync(m => m.RouteId == id);
            if (routeTrain == null)
            {
                return NotFound();
            }

            return View(routeTrain);
        }

        // POST: RouteTrains/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.RouteTrains == null)
            {
                return Problem("Entity set 'PRN211Context.RouteTrains'  is null.");
            }
            var routeTrain = await _context.RouteTrains.FindAsync(id);
            if (routeTrain != null)
            {
                _context.RouteTrains.Remove(routeTrain);
            }
            
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool RouteTrainExists(int id)
        {
          return (_context.RouteTrains?.Any(e => e.RouteId == id)).GetValueOrDefault();
        }
    }
}
