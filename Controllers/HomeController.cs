﻿using G4.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System.Diagnostics;

namespace G4.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly PRN211Context _context;

        public HomeController(ILogger<HomeController> logger, PRN211Context context)
        {
            _context = context;
            _logger = logger;
        }

        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        [HttpGet]
        public IActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Login(Account acc, Exception exception)
        {
            try
            {
                Account a = _context.Accounts.FirstOrDefault(x => x.Username == acc.Username && x.Password == acc.Password);
                if (a == null)
                {
                    throw exception;
                }
                a.Login();
            }
            catch (Exception e)
            {
                ViewBag.Error = "Login Fail";
                return View();
            }
            return RedirectToAction("Index", "Home");
        }

        public IActionResult Logout()
        {
            Account acc = new Account();
            acc.Logout();
            return RedirectToAction("Index", "Home");
        }
    }
}