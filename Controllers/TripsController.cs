﻿using G4.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace G4.Controllers
{
    public class TripsController : Controller
    {
        private readonly PRN211Context _context;

        public TripsController(PRN211Context context)
        {
            _context = context;
        }

        // GET: Trips
        public async Task<IActionResult> Index()
        {
            var pRN211Context = _context.Trips.Include(t => t.Route.End).Include(t => t.Wagon.Class).Include(t => t.Route.Start).Include(t => t.Route).Include(t => t.Wagon).Include(t => t.Wagon.Train);
            return View(await pRN211Context.ToListAsync());
        }

        // GET: Trips/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null || _context.Trips == null)
            {
                return NotFound();
            }

            var trip = await _context.Trips
                .Include(t => t.Route)
                .Include(t => t.Wagon)
                .FirstOrDefaultAsync(m => m.TripId == id);
            if (trip == null)
            {
                return NotFound();
            }


            return View(trip);
        }

        // GET: Trips/Create
        public IActionResult Create(int? wagonId, int? routeId, DateTime? date)
        {
            if (wagonId == null || routeId == null || date == null)
            {
                wagonId = _context.Wagons.FirstOrDefault().WagonId;
                routeId = _context.RouteTrains.FirstOrDefault().RouteId;
                date = date ?? DateTime.UtcNow;
            }
            var existingSlots = _context.Trips
            .Where(s => s.WagonId == wagonId && s.RouteId == routeId)
            .Select(s => s.Slot)
            .Distinct()
            .ToList();
            var validSlots = Enumerable.Range(1, 12).Select(s => (int?)s).Except(existingSlots).ToList();

            ViewData["Slot"] = new SelectList(validSlots);
            ViewData["RouteId"] = new SelectList(_context.RouteTrains, "RouteId", "RouteId");
            ViewData["WagonId"] = new SelectList(_context.Wagons, "WagonId", "WagonId");
            return View();
        }

        // POST: Trips/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("TripId,Slot,WagonId,Price,Date,RouteId,Status")] Trip trip)
        {
            if (ModelState.IsValid)
            {
                _context.Add(trip);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            ViewData["RouteId"] = new SelectList(_context.RouteTrains, "RouteId", "RouteId", trip.RouteId);
            ViewData["WagonId"] = new SelectList(_context.Wagons, "WagonId", "WagonId", trip.WagonId);
            return View(trip);
        }

        // GET: Trips/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null || _context.Trips == null)
            {
                return NotFound();
            }

            var trip = await _context.Trips.FindAsync(id);
            if (trip == null)
            {
                return NotFound();
            }
            int? wagonId = trip.WagonId;
            int? routeId = trip.RouteId;
            if (wagonId == null || routeId == null)
            {

                wagonId = _context.Wagons.FirstOrDefault().WagonId;
                routeId = _context.RouteTrains.FirstOrDefault().RouteId;
            }
            var existingSlots = _context.Trips
                .Where(s => s.TripId != id && s.WagonId == wagonId && s.RouteId == routeId && s.Date == trip.Date)
                .Select(s => s.Slot)
                .Distinct()
                .ToList();

            var validSlots = Enumerable.Range(1, 12).Select(s => (int?)s).Except(existingSlots).ToList();

            ViewData["Slot"] = new SelectList(validSlots);
            ViewData["Wagon"] = new SelectList(_context.Wagons, "WagonId", "Name", wagonId);
            ViewData["Route"] = new SelectList(_context.RouteTrains, "RouteId", "RouteId", routeId);

            return View(trip);
        }
        [HttpGet]
        public IActionResult GetAvailableSlots(int wagonId, int routeId, DateTime date)
        {
            var existingSlots = _context.Trips
                .Where(s => s.WagonId == wagonId && s.RouteId == routeId && s.Date == date)
                .Select(s => s.Slot)
                .Distinct()
                .ToList();
            var validSlots = Enumerable.Range(1, 12).Select(s => (int?)s).Except(existingSlots).ToList();

            return Json(validSlots);
        }

        // POST: Trips/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("TripId,Slot,WagonId,Price,Date,RouteId,Status")] Trip trip)
        {
            if (id != trip.TripId)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    var tripBefore = _context.Trips
                        .Include(t => t.Wagon)
                        .Include(t => t.Route)
                        .FirstOrDefault(s => s.TripId == id);
                    tripBefore.Slot = trip.Slot;
                    tripBefore.Price = trip.Price;
                    tripBefore.RouteId = trip.RouteId;

                    var routeTrain = _context.RouteTrains
                        .FirstOrDefault(r => r.RouteId == tripBefore.RouteId);
                    if (routeTrain != null)
                    {
                        tripBefore.Route = routeTrain;
                    }
                    _context.Update(tripBefore);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!TripExists(trip.TripId))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            ViewData["RouteId"] = new SelectList(_context.RouteTrains, "RouteId", "RouteId", trip.RouteId);
            ViewData["WagonId"] = new SelectList(_context.Wagons, "WagonId", "WagonId", trip.WagonId);
            return View(trip);
        }

        // GET: Trips/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null || _context.Trips == null)
            {
                return NotFound();
            }

            var trip = await _context.Trips
                .Include(t => t.Route)
                .Include(t => t.Wagon)
                .FirstOrDefaultAsync(m => m.TripId == id);
            if (trip == null)
            {
                return NotFound();
            }

            return View(trip);
        }

        // POST: Trips/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            if (_context.Trips == null)
            {
                return Problem("Entity set 'PRN211Context.Trips'  is null.");
            }
            var trip = await _context.Trips.FindAsync(id);
            if (trip != null)
            {
                _context.Trips.Remove(trip);
            }

            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool TripExists(int id)
        {
            return (_context.Trips?.Any(e => e.TripId == id)).GetValueOrDefault();
        }
    }
}
